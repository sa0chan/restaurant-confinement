import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {RouterModule, Routes} from '@angular/router';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import { HomeComponent } from './component/home/home.component';
import { UserComponent } from './component/Users/user/user.component';
import {RestaurantService} from './services/Restaurant.service';
import { CreateUserComponent } from './component/Users/create-user/create-user.component';
import {FormsModule} from '@angular/forms';
import { PlatsComponent } from './component/Plats/plats/plats.component';
import { LoginComponent } from './component/Users/login/login.component';
import {Interceptor} from './Interceptor/Interceptor';
import { PanierComponent } from './component/panier/panier.component';
import {PanierService} from './services/Panier.service';

const appRoutes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'create-user', component: CreateUserComponent},
  {path: 'panier', component: PanierComponent},
  {path: 'cartes', component: PlatsComponent},
  {path: 'login', component: LoginComponent}
];

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    UserComponent,
    CreateUserComponent,
    PlatsComponent,
    LoginComponent,
    PanierComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot(
      appRoutes
    ),
    FormsModule,
  ],
  providers: [RestaurantService,  { provide: HTTP_INTERCEPTORS, useClass: Interceptor, multi: true }, PanierService],
  bootstrap: [AppComponent]
})
export class AppModule { }
