import {EventEmitter, Injectable} from '@angular/core';
import {Subject} from 'rxjs';

@Injectable()
export class PanierService {

  public basketUpload = new EventEmitter<string[]>();


  public putItemIntoBasket(event) {
    this.basketUpload.emit(event);
    localStorage.setItem('basket', JSON.stringify(event));
  }


  public getItemFromBasket() {
    const items = localStorage.getItem('basket');
    return JSON.parse(items);
  }
}
