import {EventEmitter, Injectable} from '@angular/core';

@Injectable()
export class CommandeService {

  public commandeUpload = new EventEmitter<string[]>();


  public putItemIntoCommande(event) {
    this.commandeUpload.emit(event);
    localStorage.setItem('Commande', JSON.stringify(event));
  }


  public getItemFromCommande() {
    const items = localStorage.getItem('Commande');
    return JSON.parse(items);
  }
}
