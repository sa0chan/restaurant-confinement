import {NewUserModel} from '../models/Users/NewUser.model';
import {UserModel} from '../models/Users/User.model';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {EventEmitter, Injectable} from '@angular/core';
import {PlatsModel} from '../models/Plats.model';
import {CategoriesModel} from '../models/Categories.model';

@Injectable()
export class RestaurantService {
  constructor(private http: HttpClient) {
  }

// interceptor va se charger de rajouter http:/localhost:8080
  private PATH = '/users';

  changeConnection = new EventEmitter<boolean>();

  prependUrl(url: string) {
    return this.PATH + url;
  }

  createUtilisateur(body): Observable<NewUserModel> {
    const url = this.prependUrl('/:register');
    return this.http.post<NewUserModel>(url, body);
  }


// LOGIN USER
  loginUser(username: string, password: string): Observable<UserModel> {
    if (username) {
      this.putIdentifiersInLocalStorage(username, password);
    }
    const url = this.prependUrl(`/login/${this.getLogin()}`);
    return this.http.get<UserModel>(url);
  }

  isConnected(): boolean {
    return localStorage.getItem('isConnected') === 'true';
  }

  getLogin(): string {
    return localStorage.getItem('login');
  }

  putIdentifiersInLocalStorage(username: string, password: string): void {
    const base64 = btoa(`${username}:${password}`);
    localStorage.setItem('isConnected', 'true');
    localStorage.setItem('login', username);
    localStorage.setItem('authorization', `Basic ${base64}`);
  }

  disconnect(): void {
    localStorage.removeItem('basket');
    localStorage.removeItem('isConnected');
    localStorage.removeItem('login');
    localStorage.removeItem('authorization');
    this.changeConnection.emit(false);
  }


// -----------------------------------------------------------------------------------------------------------------
  // PLATS

  getListPlats(): Observable<PlatsModel[]> {
    return this.http.get<PlatsModel[]>(`/nourritures`);
  }

  getPlatById(id): Observable<PlatsModel> {
    return this.http.get<PlatsModel>(`/nourritures/${id}`);
  }


  // -----------------------------------------------------------------------------
  // CATEGORIE

  getListCategorie(): Observable<CategoriesModel[]> {
    return this.http.get<CategoriesModel[]>(`/categories`);
  }

  getCategorieById(id): Observable<CategoriesModel> {
    return this.http.get<CategoriesModel>(`/categories/${id}`);
  }
}


// ------------------------------------------------------------------------------------
// COMMANDE

createCommande():Observable<CommandeModel[]>{
  return this.http.post<CommandeModel[]>(body);
}
