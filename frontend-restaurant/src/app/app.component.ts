import {Component, OnInit} from '@angular/core';
import {RestaurantService} from './services/Restaurant.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'frontend-restaurant';
  connected: boolean;

  constructor(private service: RestaurantService, private  router: Router) {
  }

  ngOnInit() {
    this.connected = this.service.isConnected();
    if (this.connected) {
      this.router.navigate([`/users/login/${this.service.getLogin()}`]);
    }
    this.service.changeConnection.subscribe((b: boolean) => {
      this.connected = b;
      if (this.connected) {
        this.router.navigate([`/users/login/${this.service.getLogin()}`]);
      }
    });
  }

  deconnexion() {
    this.service.disconnect();
  }
}
