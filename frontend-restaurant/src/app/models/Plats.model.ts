export class PlatsModel {

  id: string;
  nom: string;
  prix: string;
  idCategorie: string;

  constructor(id: string, nom: string, prix: string, idCategorie: string) {
    this.id = id;
    this.nom = nom;
    this.prix = prix;
    this.idCategorie = idCategorie;
  }
}
