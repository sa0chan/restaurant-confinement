import {Timestamp} from 'rxjs';

export class PlatsModel {

  id: string;
  date: Timestamp;
  idUser: string;
  prix: string;
  listPlat: PlatsModel[];

  constructor(id: string, nom: string, prix: string, idCategorie: string) {
    this.id = id;
    this.nom = nom;
    this.prix = prix;
    this.idCategorie = idCategorie;
  }
}
