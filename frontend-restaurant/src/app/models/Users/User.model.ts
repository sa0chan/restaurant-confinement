export class UserModel {

  id: string;
  nom: string;
  prenom: string;
  email: string;
  numeroTelephone: string;
  numeroRue: number;
  nomRue: string;
  cp: number;
  ville: string;

  constructor(id: string, nom: string, prenom: string, email: string,
              numeroTelephone: string, numeroRue: number, nomRue: string, cp: number, ville: string) {
    this.id = id;
    this.nom = nom;
    this.prenom = prenom;
    this.email = email;
    this.numeroTelephone = numeroTelephone;
    this.numeroRue = numeroRue;
    this.nomRue = nomRue;
    this.cp = cp;
    this.ville = ville;
  }


}
