export class NewUserModel {

  nom: string;
  prenom: string;
  email: string;
  numeroTelephone: string;
  numeroRue: number;
  nomRue: string;
  cp: number;
  ville: string;
  password: string;

  constructor(nom: string, prenom: string, email: string,
              numeroTelephone: string, numeroRue: number, nomRue: string, cp: number, ville: string, password: string) {
    this.nom = nom;
    this.prenom = prenom;
    this.email = email;
    this.numeroTelephone = numeroTelephone;
    this.numeroRue = numeroRue;
    this.nomRue = nomRue;
    this.cp = cp;
    this.ville = ville;
    this.password = password;
  }
}
