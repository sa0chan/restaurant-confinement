import {Component, OnInit} from '@angular/core';
import {RestaurantService} from '../../../services/Restaurant.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  username: string;
  password: string;

  constructor(private service: RestaurantService, private router: Router) {
  }

  ngOnInit() {
  }

  onClickValidate() {
    console.log((this.username));
    this.service.loginUser(this.username, this.password).subscribe(
      () => this.connect(),
      () => this.disconnect()
    );
  }

  connect() {
    console.log('connecté');
    this.service.putIdentifiersInLocalStorage(this.username, this.password);
    this.service.changeConnection.emit(true);
    this.router.navigate([`/cartes`]);
  }

  disconnect() {
    console.log('deconnecté');
    this.service.disconnect();
  }

}
