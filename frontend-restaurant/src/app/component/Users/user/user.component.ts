import {Component, OnInit} from '@angular/core';
import {RestaurantService} from '../../../services/Restaurant.service';
import {UserModel} from '../../../models/Users/User.model';
import {Router} from '@angular/router';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  connected: boolean;

  constructor(private service: RestaurantService, private  router: Router) {
  }

  ngOnInit() {
    this.connected = this.service.isConnected();
    if (this.connected) {
      this.router.navigate([`/users/login/${this.service.getLogin()}`]);
    }
    this.service.changeConnection.subscribe((b: boolean) => {
      this.connected = b;
      if (this.connected) {
        this.router.navigate([`/users/login/${this.service.getLogin()}`]);
      }
    });
  }

}
