import {Component, OnInit} from '@angular/core';
import {RestaurantService} from '../../../services/Restaurant.service';
import {NewUserModel} from '../../../models/Users/NewUser.model';

@Component({
  selector: 'app-create-user',
  templateUrl: './create-user.component.html',
  styleUrls: ['./create-user.component.css']
})
export class CreateUserComponent implements OnInit {

  nom: string;
  prenom: string;
  telephone: string;
  email: string;
  password: string;
  passwordTest: string;
  numRue: number;
  nomRue: string;
  cp: number;
  ville: string;
  isemailCorrect: boolean;

  constructor(private service: RestaurantService) {
  }

  ngOnInit() {
  }

  public enregistrer() {
    const body = new NewUserModel(this.nom, this.prenom, this.email,
      this.telephone, this.numRue, this.nomRue, this.cp, this.ville, this.password);
    this.service.createUtilisateur(body).subscribe();

  }


  public validEmail(email) {
    const filter = /^\s*[\w\-\+_]+(\.[\w\-\+_]+)*\@[\w\-\+_]+\.[\w\-\+_]+(\.[\w\-\+_]+)*\s*$/;
    this.isemailCorrect = String(email).search(filter) !== -1;
    return this.isemailCorrect;
  }
}
