import {Component, Input, OnInit} from '@angular/core';
import {PanierService} from '../../services/Panier.service';
import {RestaurantService} from '../../services/Restaurant.service';
import {Router} from '@angular/router';
import {PlatsModel} from '../../models/Plats.model';
import {isEmpty} from 'rxjs/operators';

@Component({
  selector: 'app-panier',
  templateUrl: './panier.component.html',
  styleUrls: ['./panier.component.css']
})
export class PanierComponent implements OnInit {

  basket: string[];
  representationIDBasket: string[];
  finalRepresentationBasket: PlatsModel[];
  connected: boolean;
  quantity: number;
  price: number;
  pricePlat: number;
  totalPrice = 0;


  constructor(public panierService: PanierService, private service: RestaurantService, private router: Router) {
  }

  ngOnInit() {
    // connexion utilisateur
    this.connected = this.service.isConnected();
    // recherche des données du panier

    this.getPanierFromCarte();
  }

  // ------------------------------------- fin initialisation---------------------

  getPanierFromCarte() {
    this.basket = [];
    this.representationIDBasket = [];
    this.finalRepresentationBasket = [];
    // on va rechercher dans local storage le panier
    this.basket = this.panierService.getItemFromBasket();
    this.getRepresentationPanier();

  }

  // evite les doublons -> ce qui sera visible
  getRepresentationPanier() {
    if (this.basket !== null) {
      // console.log('panier plein');
      this.basket.forEach(platId => {
        if (!this.representationIDBasket.includes(platId)) {
          this.representationIDBasket.push(platId);
          this.getPlatsById(platId);
        }
        // return this.representationIDBasket;
      });
    } else {
      console.log('panier vide');
    }
  }

// recherche du plat dans la bdd
  getPlatsById(id) {
    this.service.getPlatById(id).subscribe(plat => {
      this.finalRepresentationBasket.push(plat);
      this.calculateTotalPriceBasket(plat.id, plat.prix);
    });
  }

  calculateTotalPriceBasket(id, prix) {
    const tabFiltrerById = this.basket.filter(panierItem => {
      return panierItem === id;
    });
    this.quantity = tabFiltrerById.length;
    this.price = prix * this.quantity;
    this.totalPrice += this.price;
  }

// quantité par article
  getHowManyFromBasket(platId: string) {
    const tabFiltrerById = this.basket.filter(panierItem => {
      return panierItem === platId;
    });
    return this.quantity = tabFiltrerById.length;
  }


}
