import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {RestaurantService} from '../../../services/Restaurant.service';
import {PlatsModel} from '../../../models/Plats.model';
import {Router} from '@angular/router';
import {CategoriesModel} from '../../../models/Categories.model';
import {PanierService} from '../../../services/Panier.service';

@Component({
  selector: 'app-plats',
  templateUrl: './plats.component.html',
  styleUrls: ['./plats.component.css']
})
export class PlatsComponent implements OnInit {

  basket: string[];

  platList: PlatsModel[];
  categorieList: CategoriesModel[];
  connected: boolean;
  idCategorieList: string[];
  categorieChoice = null;
  quantity: number;
  total: number;

  constructor(private service: RestaurantService, private router: Router, private panierService: PanierService) {
  }

  ngOnInit() {

    // liste des plats
    this.platList = [];
    this.idCategorieList = [];
    this.basket = [];

    this.service.getListPlats().subscribe(response => {
      response.forEach(card => {
        this.platList.push(card);
      });
      this.categorieIdListConstruct();
    });

    // categories
    this.categorieList = [];
    this.service.getListCategorie().subscribe(response => {
      response.forEach(categorie =>
        this.categorieList.push(categorie));
    });

    /*    console.log(this.categorieList);
        console.log(this.platList);
        console.log(this.idCategorieList);*/


    // connexion utilisateur

    this.connected = this.service.isConnected();
    if (this.connected) {
      this.router.navigate([`/cartes`]);
    }
    this.service.changeConnection.subscribe((b: boolean) => {
      this.connected = b;
      if (this.connected) {
        this.router.navigate([`/cartes`]);
      }
    });

  }

// ----------------------fin de l'initialiation----------------------------------


  categorieIdListConstruct() {
    this.platList.forEach(plat => {
      const idCategoriePlat = plat.idCategorie;
      if (!this.idCategorieList.includes(idCategoriePlat)) {
        this.idCategorieList.push(idCategoriePlat);
      }
    });

  }

  // filtre des catégories

  filterByCategorie() {
    if (this.categorieChoice === null || this.categorieChoice === 'Toutes les catégories') {
      return this.platList;
    }
    return this.platList.filter(plat => {
      return plat.idCategorie === this.categorieChoice;
    });
  }

// Panier
  supFromBasket(platId) {

    const indexPlatId = this.basket.indexOf(platId);
    if (indexPlatId !== -1) {

      this.basket.splice(indexPlatId, 1);
      this.panierService.putItemIntoBasket(this.basket);
    } else {
      console.log('rien a retirer ');
    }

  }

  getIntoBasket(platId) {

    this.basket.push(platId);
    // this.panierService.basketUpload.emit(platId);
    this.panierService.putItemIntoBasket(this.basket);

  }

  getHowManyFromBasket(platId) {
    const tabFiltrerById = this.basket.filter(panierItem => {
      return panierItem === platId;
    });
    this.quantity = tabFiltrerById.length;
    return tabFiltrerById.length;
  }




}
