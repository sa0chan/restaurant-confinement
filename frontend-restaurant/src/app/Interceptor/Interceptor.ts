import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Injectable} from '@angular/core';
import {RestaurantService} from '../services/Restaurant.service';

@Injectable()
export class Interceptor implements HttpInterceptor {
  constructor(private service: RestaurantService) {
  }


  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const url = 'http://localhost:8080';
    let modifiedRequest: HttpRequest<any>;

    if (!this.service.isConnected()) {
      modifiedRequest = request.clone({
        url: url + request.url
      });
    } else {
      modifiedRequest = request.clone({
        url: url + request.url,
        headers: request.headers.set('authorization', localStorage.getItem('authorization'))
      });
    }
    return next.handle(modifiedRequest);
  }
}
