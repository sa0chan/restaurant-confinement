package com.restaurant.backendrestaurant.controllers;

import com.restaurant.backendrestaurant.domain.Categories;
import com.restaurant.backendrestaurant.services.CategoriesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/categories")
public class CategoriesControllers {
    private CategoriesService categoriesService;

    @Autowired
    CategoriesControllers( CategoriesService categoriesService) {
        this.categoriesService = categoriesService;

    }

    @GetMapping
    List<Categories> getAllCategories(){
        return categoriesService.getAllCategorie();
    }


    @GetMapping("/{id}")
    Optional<Categories> getCategorieById(@PathVariable("id")String id){
        return categoriesService.getCategorieById(id);
    }
}
