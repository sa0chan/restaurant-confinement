package com.restaurant.backendrestaurant.controllers;

import com.restaurant.backendrestaurant.domain.Nourritures;

import com.restaurant.backendrestaurant.representations.Nourritures.NourrituresRepresentation;
import com.restaurant.backendrestaurant.representations.Nourritures.NourrituresRepresentationMapper;
import com.restaurant.backendrestaurant.services.NourrituresService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/nourritures")
public class NourrituresControllers {

    private NourrituresService service;
    private  NourrituresRepresentationMapper mapper;


    @Autowired
    public NourrituresControllers(NourrituresService service, NourrituresRepresentationMapper mapper) {
        this.service = service;
        this.mapper = mapper;
    }

    @GetMapping
    List<NourrituresRepresentation> getAllNourritures() {

        return this.service.getAllNourritures().stream()
                .map(this.mapper::mapNourrituresToNourrituresRepresentation)
                .collect(Collectors.toList());
    }


    @GetMapping("/{id}")
    ResponseEntity<NourrituresRepresentation> getOneNourritures(@PathVariable("id") String id) {
        Optional<Nourritures> bloc = this.service.getOneNourriture(id);

        return bloc
                .map(this.mapper::mapNourrituresToNourrituresRepresentation)
                .map(ResponseEntity::ok)
                .orElseGet(() -> ResponseEntity.notFound().build());
    }


    @PostMapping
    NourrituresRepresentation createNourriture(@RequestBody NourrituresRepresentation body){
        final Nourritures nourritures;
        nourritures = this.mapper.mapNourrituresRepresentationToNourriture(body);
        this.service.createNourriture(nourritures);
        return this.mapper.mapNourrituresToNourrituresRepresentation(nourritures);

    }


    @DeleteMapping("/{id}")
    void deleteNourriturebyId(@PathVariable("id") String id) {
        this.service.deleteNourriture(id);
    }

}
