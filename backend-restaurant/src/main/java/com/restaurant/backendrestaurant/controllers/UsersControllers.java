package com.restaurant.backendrestaurant.controllers;

import com.restaurant.backendrestaurant.domain.Users;
import com.restaurant.backendrestaurant.representations.Users.UsersRepresentation;
import com.restaurant.backendrestaurant.representations.Users.UsersRepresentationFullInfo;
import com.restaurant.backendrestaurant.representations.Users.UsersRepresentationMapper;
import com.restaurant.backendrestaurant.services.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/users")
public class UsersControllers {

    private UsersService service;
    private UsersRepresentationMapper mapper;



    @Autowired
    public UsersControllers( UsersService service, UsersRepresentationMapper mapper) {
        this.service = service;
        this.mapper = mapper;
    }


    @GetMapping("/login/{username}")
    public UsersRepresentation login(@PathVariable("username") String username) {
        try {
            return this.service.login(username);
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Username does not match authentication", e);
        }

    }


    @PostMapping("/:register")
    public UsersRepresentation createUtilisateur(@RequestBody UsersRepresentationFullInfo user) {
        final Users utilisateurs;
        utilisateurs = this.mapper.mapUtilisateurRepresentationToUtilisateur(user);
        this.service.createUser(utilisateurs);
        return this.mapper.mapUserToUserRepresentation(utilisateurs);
    }



    @GetMapping
    List<UsersRepresentation> getAllUsers() {

        return this.service.getAllUsers().stream()
                .map(this.mapper::mapUserToUserRepresentation)
                .collect(Collectors.toList());
    }


    @GetMapping("/{id}")
    ResponseEntity<UsersRepresentation> getOneUser(@PathVariable("id") String id) {
        Optional<Users> bloc = this.service.getOneUser(id);

        return bloc
                .map(this.mapper::mapUserToUserRepresentation)
                .map(ResponseEntity::ok)
                .orElseGet(() -> ResponseEntity.notFound().build());
    }


    @DeleteMapping("/{id}")
    void deleteUsersbyId(@PathVariable("id") String id) {
        this.service.deleteUser(id);
    }

}
