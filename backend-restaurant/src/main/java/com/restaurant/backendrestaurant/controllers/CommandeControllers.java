package com.restaurant.backendrestaurant.controllers;

import com.restaurant.backendrestaurant.domain.Commande;
import com.restaurant.backendrestaurant.domain.Nourritures;
import com.restaurant.backendrestaurant.representations.Commande.CommandeRepresentation;
import com.restaurant.backendrestaurant.representations.Commande.CommandeRepresentationMapper;
import com.restaurant.backendrestaurant.representations.Nourritures.NourrituresRepresentation;
import com.restaurant.backendrestaurant.services.CommandeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/commande")
public class CommandeControllers {

    private CommandeService commandeService;
    private CommandeRepresentationMapper mapper;

    @Autowired
    CommandeControllers(CommandeService commandeService, CommandeRepresentationMapper mapper) {
        this.commandeService = commandeService;
        this.mapper = mapper;


    }

    @GetMapping
    List<Commande> getAllCommande() {
        return commandeService.getAllCommandes();
    }


    @GetMapping("/{id}")
    Optional<Commande> getCommandeById(@PathVariable("id") String id) {
        return commandeService.getOneCommande(id);
    }

    @PostMapping
    CommandeRepresentation createCommande(@RequestBody CommandeRepresentation body) {
        final Commande commande;
        commande = this.mapper.mapCommandeRepresentationToCommande(body);
        this.commandeService.createCommande(commande);
        return this.mapper.mapCommandeToCommandeRepresentation(commande);

    }
}
