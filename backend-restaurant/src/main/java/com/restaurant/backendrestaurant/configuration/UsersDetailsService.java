package com.restaurant.backendrestaurant.configuration;

import com.restaurant.backendrestaurant.domain.Users;
import com.restaurant.backendrestaurant.services.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Component
public class UsersDetailsService implements UserDetailsService {

    private UsersService userService;

    @Autowired
    public UsersDetailsService(UsersService userService) {
        this.userService = userService;
    }


    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Users user = this.userService.findByUsername(username);
        return new org.springframework.security.core.userdetails.User(user.getNom(), user.getPassword(),
                List.of());
    }


}
