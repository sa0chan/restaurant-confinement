package com.restaurant.backendrestaurant.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;

@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    // dao veut dire base de donnée ou persistence comment garder les utilisateurs entre 2
    private DaoAuthenticationProvider authProvider;


    @Autowired
    public WebSecurityConfig(DaoAuthenticationProvider authProvider) {
        this.authProvider = authProvider;
    }

    //mettre le plus général en bas et le
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()
                .cors().and()
                .csrf().disable()
                //les requetes autorisées
                .authorizeRequests()
                .antMatchers("/users/:register").permitAll()
                .antMatchers(HttpMethod.GET, "/users").permitAll()
                .antMatchers(HttpMethod.GET, "/nourritures").permitAll()
                .antMatchers(HttpMethod.GET, "/categories").permitAll()
                .antMatchers(HttpMethod.GET, "/nourritures/**").permitAll()

                //.antMatchers(HttpMethod.POST,"/nourritures").permitAll()
                //toute les requetes non interceptées par les lignes avant doivent être authentifiées
                .anyRequest().authenticated().and().httpBasic();
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) {
        auth.authenticationProvider(this.authProvider);
    }


}



