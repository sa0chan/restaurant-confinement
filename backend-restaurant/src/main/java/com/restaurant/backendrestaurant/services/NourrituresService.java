package com.restaurant.backendrestaurant.services;
import com.restaurant.backendrestaurant.domain.Nourritures;
import com.restaurant.backendrestaurant.repositories.NourrituresRepository;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@Component
public class NourrituresService {

    IdGenerateur idGenerateur;
    private NourrituresRepository nourrituresRepository;

    public NourrituresService(IdGenerateur idGenerateur, NourrituresRepository nourrituresRepository) {
        this.idGenerateur = idGenerateur;
        this.nourrituresRepository = nourrituresRepository;
    }

    public List<Nourritures> getAllNourritures() {
        List<Nourritures> nourritureList = new ArrayList<>();
        nourrituresRepository.findAll().forEach(nourritureList::add);
        return nourritureList;

    }

    public Optional<Nourritures> getOneNourriture(String id) {
        return nourrituresRepository.findById(id);
    }

    public Nourritures createNourriture(Nourritures nourritures) {
        nourritures.setId(idGenerateur.generateNewId());
        nourrituresRepository.save(nourritures);
        return nourritures;
    }


    public void deleteNourriture(String id) {
        nourrituresRepository.deleteById(id);
    }
}
