package com.restaurant.backendrestaurant.services;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

@Component
public class HashedPasswordGenerator {

    public HashedPasswordGenerator() {
    }

   protected String HashThePassword(String passwordToHash){
       String password = passwordToHash;
       BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
       String hashedPassword = passwordEncoder.encode(password);
       return hashedPassword;
   }
}
