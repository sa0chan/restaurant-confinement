package com.restaurant.backendrestaurant.services;

import com.restaurant.backendrestaurant.domain.Categories;
import com.restaurant.backendrestaurant.domain.Nourritures;
import com.restaurant.backendrestaurant.repositories.CategoriesRepository;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Component

public class CategoriesService {
    CategoriesRepository categoriesRepository;

    CategoriesService(CategoriesRepository categoriesRepository){
        this.categoriesRepository = categoriesRepository;

    }

    public List<Categories> getAllCategorie(){
        List<Categories> categoriesList = new ArrayList<>();
        categoriesRepository.findAll().forEach(categoriesList:: add);
        return categoriesList;
    }


   public Optional<Categories> getCategorieById(String id){
       return categoriesRepository.findById(id);
    }

}
