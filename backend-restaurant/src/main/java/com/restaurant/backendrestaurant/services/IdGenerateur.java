package com.restaurant.backendrestaurant.services;

import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public class IdGenerateur {

    public String generateNewId() {
        return UUID.randomUUID().toString();
    }
}


