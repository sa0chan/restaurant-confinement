package com.restaurant.backendrestaurant.services;

import com.restaurant.backendrestaurant.domain.Users;
import com.restaurant.backendrestaurant.repositories.UsersRepository;
import com.restaurant.backendrestaurant.representations.Users.UsersRepresentation;
import com.restaurant.backendrestaurant.representations.Users.UsersRepresentationMapper;
import org.slf4j.Logger;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Component

public class UsersService {

    IdGenerateur idGenerateur;
    HashedPasswordGenerator hashedPasswordGenerator;
    private UsersRepository usersRepository;
    private UsersRepresentationMapper usersRepresentationMapper;



    public UsersService(UsersRepresentationMapper usersRepresentationMapper,  IdGenerateur idGenerateur, UsersRepository usersRepository, HashedPasswordGenerator hashedPasswordGenerator) {
        this.idGenerateur = idGenerateur;
        this.usersRepository = usersRepository;
        this.hashedPasswordGenerator = hashedPasswordGenerator;
        this.usersRepresentationMapper = usersRepresentationMapper;
    }

    public List<Users> getAllUsers() {
        List<Users> userList = new ArrayList<>();
        usersRepository.findAll().forEach(userList::add);
        return userList;

    }

    public Optional<Users> getOneUser(String id) {
        return usersRepository.findById(id);
    }

    @Transactional(rollbackOn = Exception.class)
    public Users createUser(Users users) {
        users.setId(idGenerateur.generateNewId());
        users.setPassword(hashedPasswordGenerator.HashThePassword(users.getPassword()));
        usersRepository.save(users);
        return users;
    }

    public void deleteUser(String id) {
        usersRepository.deleteById(id);
    }

    public Users findByUsername(String username) {
        return this.usersRepository.findByNom(username)
                .orElseThrow(() -> {
                    throw new UsernameNotFoundException(String.format("Username %s does not exist.", username));
                });

    }

    public UsersRepresentation login(String username) {
        String authUsername = this.getCurrentUsername();

        if (!authUsername.equals(username)) {
            throw new IllegalArgumentException("Username does not match authentication");
        }

        return usersRepresentationMapper.mapUserToUserRepresentation(this.findByUsername(username));
    }

    String getCurrentUsername() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return ((UserDetails) authentication.getPrincipal()).getUsername();
    }


}
