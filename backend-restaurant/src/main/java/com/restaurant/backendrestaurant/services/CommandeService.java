package com.restaurant.backendrestaurant.services;

import com.restaurant.backendrestaurant.domain.Commande;
import com.restaurant.backendrestaurant.domain.Nourritures;
import com.restaurant.backendrestaurant.repositories.CommandeRepository;
import com.restaurant.backendrestaurant.repositories.NourrituresRepository;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Component

public class CommandeService {

    IdGenerateur idGenerateur;
    private CommandeRepository commandeRepository;

    public CommandeService(IdGenerateur idGenerateur, CommandeRepository commandeRepository) {
        this.idGenerateur = idGenerateur;
        this.commandeRepository = commandeRepository;
    }

    public List<Commande> getAllCommandes() {
        List<Commande> commandeList = new ArrayList<>();
        commandeRepository.findAll().forEach(commandeList::add);
        return commandeList;

    }

    public Optional<Commande> getOneCommande(String id) {
        return commandeRepository.findById(id);
    }

    public Commande createCommande(Commande commande) {

        Date date = new Date();
        commande.setId(idGenerateur.generateNewId());
        commande.setDateCommande(date);
        commandeRepository.save(commande);
        return commande;
    }


    public void deleteCommande(String id) {
        commandeRepository.deleteById(id);
    }
}
