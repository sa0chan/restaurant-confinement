package com.restaurant.backendrestaurant.repositories;

import com.restaurant.backendrestaurant.domain.Users;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface UsersRepository extends CrudRepository<Users, String> {
    Optional<Users> findById(String id);
    Optional<Users> findByNom(String nom);
}
