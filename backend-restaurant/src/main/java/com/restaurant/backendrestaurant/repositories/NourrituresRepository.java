package com.restaurant.backendrestaurant.repositories;
import com.restaurant.backendrestaurant.domain.Nourritures;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface NourrituresRepository extends CrudRepository<Nourritures, String> {
    Optional<Nourritures> findById(String id);
}
