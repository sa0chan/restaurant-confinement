package com.restaurant.backendrestaurant.repositories;
import com.restaurant.backendrestaurant.domain.Commande;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface CommandeRepository extends CrudRepository<Commande, String> {

    Optional<Commande> findById(String id);

}
