package com.restaurant.backendrestaurant.repositories;

import com.restaurant.backendrestaurant.domain.Categories;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface CategoriesRepository extends CrudRepository<Categories, String> {
    Optional<Categories> findById(String id);

}
