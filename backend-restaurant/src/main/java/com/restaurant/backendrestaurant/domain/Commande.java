package com.restaurant.backendrestaurant.domain;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
public class Commande {
    @Id
    String id;
    @Column(name ="date_commande")
    Date dateCommande;
    Float prix;
    @OneToOne
    @JoinColumn(name = "id_user")
    Users idUsers;
    @ManyToMany
    @JoinTable(name = "listeDesPlatsCommandes",
            joinColumns = @JoinColumn(name = "id_commande"),
            inverseJoinColumns = @JoinColumn(name = "id_plat"))
    List<Nourritures> nourrituresList;

    protected  Commande(){}

    public Commande(String id, Date dateCommande, Float prix, Users idUsers, List<Nourritures> nourrituresList) {
        this.id = id;
        this.dateCommande = dateCommande;
        this.prix = prix;
        this.idUsers = idUsers;
        this.nourrituresList = nourrituresList;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getDateCommande() {
        return dateCommande;
    }

    public void setDateCommande(Date dateCommande) {
        this.dateCommande = dateCommande;
    }

    public Float getPrix() {
        return prix;
    }

    public void setPrix(Float prix) {
        this.prix = prix;
    }

    public Users getIdUsers() {
        return idUsers;
    }

    public void setIdUsers(Users idUsers) {
        this.idUsers = idUsers;
    }

    public List<Nourritures> getNourrituresList() {
        return nourrituresList;
    }

    public void setNourrituresList(List<Nourritures> nourrituresList) {
        this.nourrituresList = nourrituresList;
    }
}
