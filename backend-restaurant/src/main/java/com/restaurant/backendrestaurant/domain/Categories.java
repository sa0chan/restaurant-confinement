package com.restaurant.backendrestaurant.domain;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class  Categories {

    @Id
    String id;
    String nom;
    //Constructeur

    protected Categories(){}

    Categories(String id , String nom){
        this.nom = nom ;
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }
}
