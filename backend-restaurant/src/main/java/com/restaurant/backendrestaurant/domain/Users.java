package com.restaurant.backendrestaurant.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "users")

public class Users {

    @Id
    String id;
    String nom;
    String prenom;
    String password;
    String email;
   // @Column(name = "numero_telephone")
    String numeroTelephone;
  //  @Column(name = "numero_rue")
    Integer numeroRue;
  // @Column(name = "nom_rue")
    String nomRue;
    @Column(name = "code_postal")
    Integer cp;
    String ville;

    protected Users(){

    }

    public Users(String id, String nom, String prenom, String password, String email, String numeroTelephone, Integer numeroRue, String nomRue, Integer cp, String ville) {
        this.id = id;
        this.nom = nom;
        this.prenom = prenom;
        this.password = password;
        this.email = email;
        this.numeroTelephone = numeroTelephone;
        this.numeroRue = numeroRue;
        this.nomRue = nomRue;
        this.cp = cp;
        this.ville = ville;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNumeroTelephone() {
        return numeroTelephone;
    }

    public void setNumeroTelephone(String numeroTelephone) {
        this.numeroTelephone = numeroTelephone;
    }

    public Integer getNumeroRue() {
        return numeroRue;
    }

    public void setNumeroRue(Integer numeroRue) {
        this.numeroRue = numeroRue;
    }

    public String getNomRue() {
        return nomRue;
    }

    public void setNomRue(String nomRue) {
        this.nomRue = nomRue;
    }

    public Integer getCp() {
        return cp;
    }

    public void setCp(Integer cp) {
        this.cp = cp;
    }

    public String getVille() {
        return ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }
}
