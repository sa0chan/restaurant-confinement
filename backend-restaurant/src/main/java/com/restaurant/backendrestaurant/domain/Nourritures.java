
package com.restaurant.backendrestaurant.domain;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "nourritures")
public class Nourritures {
    @Id
    String id;
    String nom;
    Double prix;

    @ManyToMany
    List<Commande> commandeList;
    @ManyToOne()
    @JoinColumn(name = "id_categories")
    Categories categories;

    protected Nourritures(){
    }

    public Nourritures(String id, String nom, Double prix, Categories categories) {
        this.id = id;
        this.nom = nom;
        this.prix = prix;
        this.categories = categories;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Double getPrix() {
        return prix;
    }

    public void setPrix(Double prix) {
        this.prix = prix;
    }

    public Categories getCategorie() {
        return categories;
    }

    public void setCategorie(Categories categories) {
        this.categories = categories;
    }
}

