package com.restaurant.backendrestaurant.representations.Commande;

import com.restaurant.backendrestaurant.domain.Commande;
import com.restaurant.backendrestaurant.domain.Users;
import com.restaurant.backendrestaurant.repositories.CommandeRepository;
import com.restaurant.backendrestaurant.repositories.NourrituresRepository;
import com.restaurant.backendrestaurant.repositories.UsersRepository;
import org.springframework.stereotype.Component;

@Component
public class CommandeRepresentationMapper {

    private CommandeRepository repository;
    private UsersRepository usersRepository;
    private NourrituresRepository nourrituresRepository;


    public CommandeRepresentationMapper(CommandeRepository repository, UsersRepository usersRepository, NourrituresRepository nourrituresRepository) {
        this.repository = repository;
        this.usersRepository = usersRepository;
        this.nourrituresRepository = nourrituresRepository;
    }

    public CommandeRepresentation mapCommandeToCommandeRepresentation(Commande commande) {
        String idUser = commande.getIdUsers().getId();
        CommandeRepresentation result =
                new CommandeRepresentation(commande.getId(), commande.getDateCommande(), commande.getPrix(), idUser, commande.getNourrituresList());
        result.setId(commande.getId());
        return result;
    }

    public Commande mapCommandeRepresentationToCommande(CommandeRepresentation commandeRepresentation) {
        Users users = usersRepository.findById(commandeRepresentation.getIdUsers()).orElseThrow();
        Commande result =
                new Commande(commandeRepresentation.getId(), commandeRepresentation.getDateCommande(), commandeRepresentation.getPrix(), users, commandeRepresentation.getNourrituresIdList());
        return result;
    }
}


