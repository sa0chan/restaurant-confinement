package com.restaurant.backendrestaurant.representations.Categories;

public class CategorieRepresentation {
    String id;
    String nom;

    protected  CategorieRepresentation(){}

    public CategorieRepresentation(String id, String nom) {
        this.id = id;
        this.nom = nom;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }
}
