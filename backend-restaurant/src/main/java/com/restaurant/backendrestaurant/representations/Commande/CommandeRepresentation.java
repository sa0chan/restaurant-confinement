package com.restaurant.backendrestaurant.representations.Commande;
import com.restaurant.backendrestaurant.domain.Nourritures;

import java.util.Date;
import java.util.List;

public class CommandeRepresentation {

    String id;
    Date dateCommande;
    Float prix;
    String idUsers;
    List<Nourritures> nourrituresIdList;

    protected  CommandeRepresentation(){}

    public CommandeRepresentation(String id, Date dateCommande, Float prix, String idUsers, List<Nourritures> nourrituresIdList) {
        this.id = id;
        this.dateCommande = dateCommande;
        this.prix = prix;
        this.idUsers = idUsers;
        this.nourrituresIdList = nourrituresIdList;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getDateCommande() {
        return dateCommande;
    }

    public void setDateCommande(Date dateCommande) {
        this.dateCommande = dateCommande;
    }

    public Float getPrix() {
        return prix;
    }

    public void setPrix(Float prix) {
        this.prix = prix;
    }

    public String getIdUsers() {
        return idUsers;
    }

    public void setIdUsers(String idUsers) {
        this.idUsers = idUsers;
    }

    public List<Nourritures> getNourrituresIdList() {
        return nourrituresIdList;
    }

    public void setNourrituresIdList(List<Nourritures> nourrituresIdList) {
        this.nourrituresIdList = nourrituresIdList;
    }
}
