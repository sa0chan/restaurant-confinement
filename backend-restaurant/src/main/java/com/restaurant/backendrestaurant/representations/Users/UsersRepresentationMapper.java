package com.restaurant.backendrestaurant.representations.Users;

import com.restaurant.backendrestaurant.domain.Users;
import com.restaurant.backendrestaurant.repositories.UsersRepository;
import org.springframework.stereotype.Component;

@Component
public class UsersRepresentationMapper {


        private UsersRepository repository;


        public UsersRepresentationMapper(UsersRepository repository) {
            this.repository = repository;
        }

        public UsersRepresentation mapUserToUserRepresentation(Users utilisateur) {

            UsersRepresentation result =
                    new UsersRepresentation(utilisateur.getId(), utilisateur.getNom(),utilisateur.getPrenom(),
                            utilisateur.getEmail(),utilisateur.getNumeroTelephone(),
                            utilisateur.getNumeroRue(), utilisateur.getNomRue(),utilisateur.getCp(), utilisateur.getVille());
            result.setId(utilisateur.getId());
            return result;
        }

        public Users mapUtilisateurRepresentationToUtilisateur(UsersRepresentationFullInfo utilisateurRepresentation) {
            Users result =
                    new Users(utilisateurRepresentation.getId(),utilisateurRepresentation.getNom(),utilisateurRepresentation.getPrenom(),
                             utilisateurRepresentation.getPassword(),utilisateurRepresentation.getEmail(),utilisateurRepresentation.getNumeroTelephone(),
                            utilisateurRepresentation.getNumeroRue(), utilisateurRepresentation.getNomRue(),utilisateurRepresentation.getCp(), utilisateurRepresentation.getVille());
            return result;
        }



}
