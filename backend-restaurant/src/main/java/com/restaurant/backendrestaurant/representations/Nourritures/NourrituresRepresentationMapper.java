package com.restaurant.backendrestaurant.representations.Nourritures;
import com.restaurant.backendrestaurant.domain.Categories;
import com.restaurant.backendrestaurant.domain.Nourritures;
import com.restaurant.backendrestaurant.repositories.CategoriesRepository;
import com.restaurant.backendrestaurant.repositories.NourrituresRepository;
import org.springframework.stereotype.Component;

@Component
public class NourrituresRepresentationMapper {

    private NourrituresRepository repository;
    private CategoriesRepository categoriesRepository;


    public NourrituresRepresentationMapper(NourrituresRepository repository, CategoriesRepository categoriesRepository) {
        this.repository = repository;
        this.categoriesRepository = categoriesRepository;
    }

    public NourrituresRepresentation mapNourrituresToNourrituresRepresentation(Nourritures nourritures) {
        NourrituresRepresentation result =
                new NourrituresRepresentation(nourritures.getId(), nourritures.getNom(), nourritures.getCategorie().getId(), nourritures.getPrix());
        result.setId(nourritures.getId());
        return result;
    }

    public Nourritures mapNourrituresRepresentationToNourriture(NourrituresRepresentation nourrituresRepresentation) {
        Categories categories = categoriesRepository.findById(nourrituresRepresentation.getIdCategorie()).orElseThrow();
        Nourritures result =
                new Nourritures(nourrituresRepresentation.getId(),nourrituresRepresentation.getNom(),nourrituresRepresentation.getPrix(), categories);
        return result;
    }
}
