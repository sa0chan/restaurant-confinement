package com.restaurant.backendrestaurant.representations.Nourritures;


public class NourrituresRepresentation {
    String id;
    String nom;
    String idCategories;
    Double prix;

    public NourrituresRepresentation(String id, String nom, String idCategories, Double prix) {
        this.id = id;
        this.nom = nom;
        this.idCategories = idCategories;
        this.prix = prix;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String  getIdCategorie() {
        return idCategories;
    }

    public void setIdCategorie(String idCategories) {
        this.idCategories = idCategories;
    }

    public Double getPrix() {
        return prix;
    }

    public void setPrix(Double prix) {
        this.prix = prix;
    }
}
