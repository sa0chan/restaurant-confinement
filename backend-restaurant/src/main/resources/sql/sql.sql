
 Create table users(
    id varchar NOT NULL,
    nom varchar NOT NULL,
    prenom varchar NOT NULL,
    password varchar NOT NULL,
    email varchar NOT NULL,
    numero_telephone varchar,
    numero_rue int,
    nom_rue varchar,
    code_postal varchar,
    ville varchar,
    primary key (id)
 )

 Create table categories(
 	id varchar NOT NULL,
 	nom varchar NOT NULL,
 	primary key (id)

 )

 INSERT into categories(id, nom)
  values('1','Entrée'),
  values('2','Plat'),
  values('3','Dessert');

  // pour ajouter nourritures aller dans insomna avec authentification


 Create table nourritures(
    id varchar NOT NULL,
    nom varchar NOT NULL,
    prix float NOT NULL,
    id_categories  varchar references categories(id),
     primary key (id)

 )

  Create table commande(
       id varchar NOT NULL,
       date_commande varchar,
       prix float,
       id_user varchar references users(id),
       primary key (id)
     )

      Create table listeDesPlatsCommandes(
         id_plat varchar references nourritures(id),
         id_commande varchar references commande(id)
       )

  Create table livreurs(
      id varchar NOT NULL,
      nom varchar NOT NULL,
      prenom varchar NOT NULL,
      password varchar NOT NULL,
      email varchar NOT NULL,
      primary key (id)

  )



